package com.ganlucode.sipmock.sip.mock;

import com.ganlucode.sipmock.sip.mock.entity.DeviceInfoParam;
import com.ganlucode.sipmock.sip.mock.entity.MockParam;
import com.ganlucode.sipmock.sip.request.SipRequestFactory;
import com.ganlucode.sipmock.utils.SipHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.ClientTransaction;
import javax.sip.SipStack;
import javax.sip.message.Request;

/**
 * @author GanLu
 * @date 2020-05-25 13:43
 */
@Service("deviceMock")
public class DeviceMock implements ISipMock<DeviceInfoParam> {

    @Autowired
    private SipRequestFactory sipRequestFactory;

    @Autowired
    private SipHelper sipHelper;
    @Autowired
    private SipStack sipStack;

    @Override
    public void start(MockParam<DeviceInfoParam> param) throws Exception {
        Request request = sipRequestFactory.createDeviceInfoRequest(param.getParam());
        ClientTransaction clientTransaction = sipHelper.getSipProvider()
                .getNewClientTransaction(request);
        clientTransaction.sendRequest();
    }

    @Override
    public void stop(MockParam<DeviceInfoParam> param) throws Exception {

    }
}
