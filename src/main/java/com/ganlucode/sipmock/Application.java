package com.ganlucode.sipmock;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author GanLu
 * @date 2020-05-15 10:18
 */
@Slf4j
@EnableScheduling
@EnableAsync
@SpringBootApplication
@ComponentScan(basePackages = {"com.ganlucode"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
