package com.ganlucode.sipmock.gb28181.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecordItem {

	private String recordId;

	private String deviceId;
	
	private String name;
	
	private String filePath;
	
	private String address;
	
	private Date startTime;
	
	private Date endTime;
	
	private int secrecy;
	
	private String type;

}
