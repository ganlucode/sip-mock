package com.ganlucode.sipmock.gb28181.dto;

import lombok.Data;

/**
 * @author GanLu
 * @date 2020-05-18 14:31
 */
@Data
public class RequestMessage<T> {

    private String id;

    private String deviceId;

    private String type;

    private T data;

    //FIXME
    public void setType(String type) {
        this.type = type;
        this.id = type + deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        this.id = type + deviceId;
    }

}
