package com.ganlucode.sipmock.sip.request.impl;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import com.ganlucode.sipmock.sip.request.SipRequestFactory;
import com.ganlucode.sipmock.utils.SipHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 */
@Slf4j
@Service("ackRequestProcessor")
public class AckRequestProcessor implements ISipRequestProcessor {

	@Autowired
	private SipHelper sipHelper;

	@Autowired
	private SipRequestFactory sipRequestFactory;

	/**   
	 * 处理  ACK请求
	 * @param evt
	 */  
	@Override
	public void process(RequestEvent evt) throws Exception {
		log.info("收到ACK");
//		Request request = evt.getRequest();
//		Dialog dialog = evt.getDialog();
//		Request ackRequest = null;
//		CSeq csReq = (CSeq) request.getHeader(CSeq.NAME);
//		ackRequest = dialog.createAck(csReq.getSeqNumber());
//		dialog.sendAck(ackRequest);
//		log.info("send ack to callee:{}", ackRequest.toString());

//		Request ackRequest = sipRequestFactory.createAckRequest(request);
//		log.info("回复ACK");
//		sipHelper.getSipProvider().sendRequest(ackRequest);
	}

}
