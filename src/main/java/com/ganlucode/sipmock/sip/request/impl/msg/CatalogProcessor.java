package com.ganlucode.sipmock.sip.request.impl.msg;

import com.ganlucode.sipmock.sip.mock.ISipMock;
import com.ganlucode.sipmock.sip.mock.MockHelper;
import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 * @date 2020-05-18 15:25
 */
@Slf4j
@Service("catalogProcessor")
public class CatalogProcessor extends BaseMessageProcessor implements ISipRequestProcessor {

    @Autowired
    @Qualifier("catalogMock")
    private ISipMock catalogMock;

    @Autowired
    private MockHelper mockHelper;

    /***
     * 收到catalog设备目录列表请求 处理
     * @param evt
     */
    @Override
    public void process(RequestEvent evt) throws Exception {
        log.info("CatalogProcessor:处理Catalog请求");
        catalogMock.start(mockHelper.buildCatalogMock());
    }
}
