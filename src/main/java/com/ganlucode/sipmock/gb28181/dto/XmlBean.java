package com.ganlucode.sipmock.gb28181.dto;

import com.ganlucode.sipmock.utils.XmlUtil;
import com.thoughtworks.xstream.XStream;

import java.io.InputStream;

/**
 * @author GanLu
 * @date 2020-05-20 16:37
 */
public class XmlBean {

    public String toXmlString(){
        return XmlUtil.beanToXml(this, getXstream());
    }
    public String beanToXmlWithHeader(){
        return beanToXmlWithHeader(getXstream());
    }
    public String beanToXmlWithHeader(XStream xstream){
        return XmlUtil.beanToXmlWithHeader(this,xstream);
    }
    public String beanToXmlWithHeader(XStream xstream, String version, String encoding){
        return XmlUtil.beanToXmlWithHeader(this, xstream, version, encoding);
    }

    public XStream getXstream(){
        XStream xStream = XmlUtil.getXStream(this.getClass());
//        xStream.addDefaultImplementation(getClass(),getClass().getSuperclass());
        return xStream;
    }

    public <T extends XmlBean> T fromXmlString(String xml){

        return fromXmlString(getXstream(), xml);
    }

    public <T extends XmlBean> T fromXmlString(XStream xstream, String xml){
        return (T) XmlUtil.xmlToBean(xstream, xml, this);
    }

    public <T extends XmlBean> T fromXmlString(InputStream inputStream){

        return fromXmlString(getXstream(), inputStream);
    }

    public <T extends XmlBean> T fromXmlString(XStream xstream, InputStream inputStream){
        return (T) XmlUtil.xmlToBean(xstream, inputStream, this);
    }

}
