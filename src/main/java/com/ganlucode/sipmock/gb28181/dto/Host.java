package com.ganlucode.sipmock.gb28181.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Host {
	
	private String ip;
	private int port;
	private String address;


}
