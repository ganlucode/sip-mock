package com.ganlucode.sipmock.sip.request.impl.msg;

import com.ganlucode.sipmock.service.IDeviceService;
import com.ganlucode.sipmock.service.ISipCommandService;
import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 * @date 2020-05-18 15:26
 */
@Slf4j
@Service("alarmProcessor")
public class AlarmProcessor extends BaseMessageProcessor implements ISipRequestProcessor {

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ISipCommandService sipCommandService;
    /***
     * 收到alarm设备报警信息 处理
     * @param evt
     */
    @Override
    public void process(RequestEvent evt) throws Exception {
        log.info("AlarmProcessor:处理Alarm请求");
    }
}
