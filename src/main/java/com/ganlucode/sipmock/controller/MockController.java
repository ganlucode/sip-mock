package com.ganlucode.sipmock.controller;

import com.ganlucode.sipmock.common.Result;
import com.ganlucode.sipmock.sip.mock.ISipMock;
import com.ganlucode.sipmock.sip.mock.MockHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author GanLu
 * @date 2020-05-22 17:11
 */
@RestController
@RequestMapping("/mock")
public class MockController {

    @Autowired
    private MockHelper mockHelper;

    @Autowired
    @Qualifier("registerMock")
    private ISipMock registerMock;
    @Autowired
    @Qualifier("deviceMock")
    private ISipMock deviceMock;
    @Autowired
    @Qualifier("catalogMock")
    private ISipMock catalogMock;
    @Autowired
    @Qualifier("keepAliveMock")
    private ISipMock keepAliveMock;

    @GetMapping("/register")
    public Result register() throws Exception {
        registerMock.start(null);
        return Result.success();
    }
    @GetMapping("/device-info")
    public Result deviceInfo() throws Exception {
        deviceMock.start(mockHelper.buildDeviceInfoMock());
        return Result.success();
    }

    @GetMapping("/catalog")
    public Result catalog() throws Exception {
        catalogMock.start(mockHelper.buildCatalogMock());
        return Result.success();
    }
    @GetMapping("/keep-alive/start")
    public Result startKeepAlive() throws Exception {
        keepAliveMock.start(null);
        return Result.success();
    }
    @GetMapping("/keep-alive/stop")
    public Result stopKeepAlive() throws Exception {
        keepAliveMock.stop(null);
        return Result.success();
    }

}
