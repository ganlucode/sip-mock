package com.ganlucode.sipmock.service.impl;

import com.ganlucode.sipmock.service.ISipCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FIXME
 * @author GanLu
 * @date 2020-05-19 11:26
 */
@Slf4j
@Service("redis")
public class SipCacheServiceImpl implements ISipCacheService {
    @Override
    public boolean expire(String key, long time) {
        return false;
    }

    @Override
    public long getExpire(String key) {
        return 0;
    }

    @Override
    public boolean hasKey(String key) {
        return false;
    }

    @Override
    public boolean del(String... key) {
        return false;
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        return null;
    }

    @Override
    public <T> List<T> getList(String key, Class<T> clazz) {
        return null;
    }

    @Override
    public <T> boolean set(String key, T value) {
        return false;
    }

    @Override
    public <T> boolean set(String key, T value, long time) {
        return false;
    }

    @Override
    public <T> List<T> keys(String key, Class<T> clazz) {
        return null;
    }
}
