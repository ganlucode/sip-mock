package com.ganlucode.sipmock.gb28181.dto.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author GanLu
 * @date 2020-05-20 17:01
 */
@Builder
@Data
public class DeviceXml {

    /**
     * 设备/区域/系统编码(必选)
     */
    @XStreamAlias("DeviceID")
    private String deviceId;
    /**
     * 设备/区域/系统名称(必选)
     */
    @XStreamAlias("Name")
    private String name;
    /**
     * 当为设备时,设备厂商(必选)
     */
    @XStreamAlias("Manufacturer")
    private String manufacturer;
    /**
     * 当为设备时,设备型号(必选)
     */
    @XStreamAlias("Model")
    private String model;
    /**
     * 当为设备时,设备归属(必选)
     */
    @XStreamAlias("Owner")
    private String owner;
    /**
     * 行政区域(必选)
     */
    @XStreamAlias("CivilCode")
    private String civilCode;
    /**
     * 警区(可选)
     */
    @XStreamAlias("Block")
    private String block;
    /**
     * 当为设备时,安装地址(必选)
     */
    @XStreamAlias("Address")
    private String address;
    /**
     * 当为设备时,是否有子设备(必选)1有,0没有
     */
    @XStreamAlias("Parental")
    private Integer parental = 0;
    /**
     * 父设备/区域/系统ID(必选)
     */
    @XStreamAlias("ParentID")
    private String parentId;
    /**
     * 信令安全模式(可选)缺省为0;
     * 0:不采用;
     * 2:S/MIME 签名方式;
     * 3:S/ MIME加密签名同时采用方式;
     * 4:数字摘要方式
     */
    @XStreamAlias("SafetyWay")
    private Integer safetyWay = 0;
    /**
     * 注册方式(必选)缺省为1;
     * 1:符合IETFRFC3261标准的认证注册模式;
     * 2:基于口令的双向认证注册模式;
     * 3:基于数字证书的双向认证注册模式
     */
    @XStreamAlias("RegisterWay")
    private Integer registerWay = 1;
    /**
     * 证书序列号(有证书的设备必选)
     */
    @XStreamAlias("CertNum")
    private String certNum;
    /**
     * 证书有效标识(有证书的设备必选)缺省为0;
     * 证书有效标识:
     *  0:无效
     *  1: 有效
     */
    @XStreamAlias("Certifiable")
    private Integer certifiable = 0;
    /**
     * 无效原因码(有证书且证书无效的设备必选)
     */
    @XStreamAlias("ErrCode")
    private Integer errCode = 0;
    /**
     * 证书终止有效期(有证书的设备必选)
     */
    @XStreamAlias("EndTime")
    private Date endTime;
    /**
     * 保密属性(必选)缺省为0;
     * 0:不涉密,
     * 1:涉密
     */
    @XStreamAlias("Secrecy")
    private Integer secrecy = 0;
    /**
     * 设备/区域/系统IP地址(可选)
     */
    @XStreamAlias("IPAddress")
    private String ipAddress;
    /**
     * 设备/区域/系统端口(可选)
     */
    @XStreamAlias("Port")
    private Integer port = 0;
    /**
     * 设备口令(可选)
     */
    @XStreamAlias("Password")
    private String password;
    /**
     * 设备状态(必选)
     */
    @XStreamAlias("Status")
    private String status;
    /**
     * 经度(可选)
     */
    @XStreamAlias("Longitude")
    private Double longitude = 0.00;
    /**
     * 纬度(可选)
     */
    @XStreamAlias("Latitude")
    private Double latitude = 0.00;

    /**
     * TODO: 2020-05-21
     * Info 参照文档
     */
//    private Info info;

}
