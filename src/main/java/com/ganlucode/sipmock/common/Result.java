package com.ganlucode.sipmock.common;


import lombok.Data;

/**
 * @author GanLu
 */
@Data
public class Result<T> {

    private boolean success;

    private String message;

    private Integer code;

    private T data;

    public Result(T data) {
        this(true, 0,"请求成功", data);
    }

    public Result(boolean success, Integer code, String message) {
        this(success, code,message, null);
    }

    public Result(Integer code, String message) {
        this.message = message;
        this.code = code;
    }
    public boolean getSuccess(){
            return  success;
    }
    public Result(boolean success, Integer code, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
        this.code = code;
    }

    public Result() {

    }


    public static <T> Result success(Integer code ,T data) {
//        return new ResponseResult(true, code,"SUCCESS", data);
        return new Result<>(true, code,"请求成功", data);
    }

    public static <T> Result success(T data) {
        return new Result<>(true, 0,"请求成功", data);
    }
    public static <T> Result success() {
        return new Result<>(true, 0,"请求成功", null);
    }

    public static <T> Result success(String message, T data) {
        return new Result<>(true,0, message, data);
    }

    public static <T> Result<T> failure(T data) {
        return failure(-1,"请求失败", data);
    }

    public static Result failure(String message) {
        return failure(-1, message, null);
    }

    public static Result failure(Integer code) {
        return new Result<>(false, code,"请求失败", null);
    }

    public static Result failure(Integer code,String message) {
        return new Result<>(false, code,message, null);
    }

    public static <T> Result<T> failure(Integer code,String message, T data) {
        return new Result<>(false,code, message, data);
    }





}
