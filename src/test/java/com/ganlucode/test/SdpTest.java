package com.ganlucode.test;

import com.ganlucode.sipmock.gb28181.sdp.SdpFactoryGB28181;
import com.ganlucode.sipmock.gb28181.sdp.SessionDescriptionGB28181;
import org.junit.Before;
import org.junit.Test;

import javax.sdp.SdpParseException;

/**
 * @author GanLu
 * @date 2020-05-28 10:10
 */
public class SdpTest {

    private SdpFactoryGB28181 sdpFactory;

    @Before
    public void before(){
        sdpFactory = SdpFactoryGB28181.getInstance();
    }

    @Test
    public void test() throws SdpParseException {
        String content = "v=0\r\n" +
                "o=61000000001180006001 1838 1838 IN IP4 192.168.0.208\r\n" +
                "s=Play\r\n" +
                "c=IN IP4 192.168.0.208\r\n" +
                "t=0 0\r\n" +
                "m=video 15060 TCP/RTP/AVP 96\r\n" +
                "a=setup:active\r\n" +
                "a=sendonly\r\n" +
                "a=rtpmap:96 PS/90000\r\n" +
                "a=username:61000000001180006001\r\n" +
                "a=password:123456\r\n" +
                "a=filesize:0\r\n" +
                "y=0200005702\r\n"+
                "f=";
        SessionDescriptionGB28181 sessionDescription = (SessionDescriptionGB28181) sdpFactory
                .createSessionDescription(content);
        System.out.println(sessionDescription.getSsrcField().getSsrc());
        System.out.println(sessionDescription.toString());
    }

}
