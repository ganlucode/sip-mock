package com.ganlucode.sipmock.gb28181.sdp.parser;

import com.ganlucode.sipmock.gb28181.sdp.SessionDescriptionGB28181;
import gov.nist.javax.sdp.SessionDescriptionImpl;
import gov.nist.javax.sdp.fields.SDPField;
import gov.nist.javax.sdp.parser.SDPAnnounceParser;
import gov.nist.javax.sdp.parser.SDPParser;

import java.text.ParseException;
import java.util.Vector;

/**
 * @author GanLu
 * @date 2020-05-28 10:42
 */
public class SDPAnnounceParserGB28181 extends SDPAnnounceParser {

    public SDPAnnounceParserGB28181(Vector sdpMessage) {
        super(sdpMessage);
    }

    public SDPAnnounceParserGB28181(String message) {
        super(message);
    }

    @Override
    public SessionDescriptionImpl parse() throws ParseException {
        //！！！这里用自定义协议描述
        SessionDescriptionGB28181 retval = new SessionDescriptionGB28181();
        for (int i = 0; i < sdpMessage.size(); i++) {
            String field = (String) sdpMessage.elementAt(i);
            //！！！这里用自定义转换工厂
            SDPParser sdpParser = ParserFactoryGB28181.createParser(field);
            if (sdpParser != null)
            {
                SDPField sdpField = sdpParser.parse();
                retval.addField(sdpField);
            }
        }
        return retval;

    }
}
