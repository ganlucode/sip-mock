package com.ganlucode.sipmock.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author GanLu
 * @date 2020-05-14 20:45
 */
@Data
@Configuration
public class SipServerProperties {
    @Value("${sip.server.ip}")
    private String sipIp;
    @Value("${sip.server.port}")
    private Integer sipPort;
    @Value("${sip.client.ip}")
    private String sipClientIp;
    @Value("${sip.client.port}")
    private Integer sipClientPort;
    @Value("${sip.client.transport}")
    private String clientTransport;
    @Value("${sip.domain}")
    private String sipDomain;
    @Value("${sip.server.id}")
    private String sipServerId;
    @Value("${sip.client.id}")
    private String sipClientId;
    @Value("${sip.password}")
    private String sipPassword;
    @Value("${sip.device.id}")
    private String deviceId;
}
