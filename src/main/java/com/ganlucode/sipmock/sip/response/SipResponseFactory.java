package com.ganlucode.sipmock.sip.response;

import com.ganlucode.sipmock.config.SipServerProperties;
import com.ganlucode.sipmock.utils.SipHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.address.AddressFactory;
import javax.sip.header.HeaderFactory;
import javax.sip.message.MessageFactory;

/**
 * @author GanLu
 * @date 2020-05-25 10:38
 */
@Slf4j
@Component
public class SipResponseFactory {
    @Autowired
    private SipHelper sipHelper;

    @Autowired
    private SipServerProperties sipServerProperties;

    @Autowired
    private HeaderFactory headerFactory;

    @Autowired
    private AddressFactory addressFactory;

    @Autowired
    private MessageFactory messageFactory;


//    private Response createDeviceInfoResponse(RequestEvent requestEvent){
//        Response response = messageFactory.createResponse(Response.M)
//    }
}
