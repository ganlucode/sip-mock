package com.ganlucode.sipmock.gb28181.dto.response;

import com.ganlucode.sipmock.gb28181.dto.XmlBean;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author GanLu
 * @date 2020-05-20 16:35
 *
 *
 */
@Builder
@Data
@XStreamAlias("Response")
public class CatalogResponse extends XmlBean {

    /**
     * 命令类型:设备目录查询(必选)
     * fixed="Catalog"
     */
    @XStreamAlias("CmdType")
    protected String cmdType;
    /**
     * 命令序列号(必选)
     */
    @XStreamAlias("SN")
    protected String sn;
    /**
     * 目标设备/区域/系统的编码,取值与目录查询请求相同(必选)
     */
    @XStreamAlias("DeviceID")
    protected String deviceId;
    /**
     * 查询结果总数(必选)
     */
    @XStreamAlias("SumNum")
    protected String sumNum;
    /**
     * 设备目录项列表,Num 表示目录项个数
     */
    @XStreamAlias(value = "DeviceList",impl = List.class)
    protected List<DeviceXml> deviceList;
    /**
     * 执行结果标志(必选)
     */
    @XStreamAlias("Result")
    protected String result;

    @Override
    public XStream getXstream() {
        XStream xStream = super.getXstream();
        xStream.alias("Item",DeviceXml.class);
        return xStream;
    }
}
