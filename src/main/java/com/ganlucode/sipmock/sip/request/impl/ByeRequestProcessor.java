package com.ganlucode.sipmock.sip.request.impl;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;


/**
 * @author GanLu
 */
@Slf4j
@Service("byeRequestProcessor")
public class ByeRequestProcessor implements ISipRequestProcessor {

	/**   
	 * 处理BYE请求
	 * 
	 * @param evt
	 */  
	@Override
	public void process(RequestEvent evt) throws Exception {
		log.info("ByeRequestProcessor:处理Bye");
	}

}
