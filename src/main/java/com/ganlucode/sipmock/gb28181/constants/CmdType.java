package com.ganlucode.sipmock.gb28181.constants;

/**
 * @author GanLu
 * @date 2020-05-15 16:58
 */
public final class CmdType {

    public static final String MESSAGE_CATALOG = "Catalog";
    public static final String MESSAGE_DEVICE_INFO = "DeviceInfo";
    public static final String MESSAGE_KEEP_ALIVE = "Keepalive";
    public static final String MESSAGE_ALARM = "Alarm";
    public static final String MESSAGE_RECORD_INFO = "RecordInfo";
    public static final String MESSAGE_BROADCAST = "Broadcast";
    public static final String MESSAGE_DEVICE_STATUS = "DeviceStatus";
    public static final String MESSAGE_MOBILE_POSITION = "MobilePosition";
    public static final String MESSAGE_MOBILE_POSITION_INTERVAL = "Interval";
    
}
