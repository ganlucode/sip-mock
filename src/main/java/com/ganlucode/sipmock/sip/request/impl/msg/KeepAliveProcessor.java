package com.ganlucode.sipmock.sip.request.impl.msg;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import com.ganlucode.sipmock.utils.SipHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;
import javax.sip.ServerTransaction;
import javax.sip.message.MessageFactory;
import javax.sip.message.Request;
import javax.sip.message.Response;

/**
 * @author GanLu
 * @date 2020-05-18 15:24
 */
@Slf4j
@Service("keepAliveProcessor")
public class KeepAliveProcessor extends BaseMessageProcessor implements ISipRequestProcessor {

    @Autowired
    private SipHelper sipHelper;
    @Autowired
    private MessageFactory messageFactory;
    /***
     * 收到keepalive请求 处理
     * @param evt
     */
    @Override
    public void process(RequestEvent evt) throws Exception {
        Request request = evt.getRequest();
        Response response = messageFactory.createResponse(Response.OK, request);
        ServerTransaction transaction = sipHelper.createServerTransaction(evt);
        transaction.sendResponse(response);
    }
}
