package com.ganlucode.sipmock.gb28181.sdp.parser;

import com.ganlucode.sipmock.gb28181.sdp.fields.SsrcField;
import gov.nist.core.Token;
import gov.nist.javax.sdp.fields.SDPField;
import gov.nist.javax.sdp.parser.Lexer;
import gov.nist.javax.sdp.parser.SDPParser;

import java.text.ParseException;

/**
 * @author GanLu
 * @date 2020-05-27 18:27
 */
public class SsrcFieldParser extends SDPParser {

    public SsrcFieldParser(String ssrcField) {
        this.lexer = new Lexer("charLexer", ssrcField);
    }

    public SsrcField ssrcField() throws ParseException{
        try {
            this.lexer.match('y');
            this.lexer.SPorHT();
            this.lexer.match('=');
            this.lexer.SPorHT();

            SsrcField ssrcField = new SsrcField();
            // TODO: 2020-05-27
            lexer.match(Lexer.ID);
            Token version = lexer.getNextToken();
            ssrcField.setSsrc(version.getTokenValue());
            this.lexer.SPorHT();

            return ssrcField;
        } catch (Exception e) {
            e.printStackTrace();
            throw lexer.createParseException();
        }
    }

    @Override
    public SDPField parse() throws ParseException {
        return this.ssrcField();
    }


    public static void main(String[] args) throws ParseException {
        String y = "y=0200005702";
        SsrcFieldParser ssrcFieldParser = new SsrcFieldParser(y);
        SsrcField ssrcField = (SsrcField) ssrcFieldParser.parse();
        System.out.println(ssrcField.getSsrc());
        System.out.println(ssrcField.encode());
    }


}
