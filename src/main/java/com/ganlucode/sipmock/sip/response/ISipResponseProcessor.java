package com.ganlucode.sipmock.sip.response;

import javax.sip.ResponseEvent;

/**
 * @author GanLu
 * @date 2020-05-18 16:59
 */
public interface ISipResponseProcessor {

    void process(ResponseEvent responseEvent) throws Exception;

}
