package com.ganlucode.sipmock.gb28181.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecordInfo {

	private String deviceId;
	
	private String name;
	
	private int sumNum;
	
	private List<RecordItem> recordList;

}
