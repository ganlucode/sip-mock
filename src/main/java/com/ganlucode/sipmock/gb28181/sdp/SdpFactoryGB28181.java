package com.ganlucode.sipmock.gb28181.sdp;

import com.ganlucode.sipmock.gb28181.sdp.parser.SDPAnnounceParserGB28181;

import javax.sdp.*;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;

/**
 * @author GanLu
 * @date 2020-05-28 10:27
 */
public class SdpFactoryGB28181 {

    private SdpFactory sdpFactory = SdpFactory.getInstance();

    private static final SdpFactoryGB28181 singletonInstance = new SdpFactoryGB28181();

    private SdpFactoryGB28181() {
    }

    public static SdpFactoryGB28181 getInstance() {
        return singletonInstance;
    }

    /**
     * Creates a new, empty SessionDescription. The session is set as follows:
     *
     * v=0
     *
     * o=this.createOrigin ("user", InetAddress.getLocalHost().toString());
     *
     * s=-
     *
     * t=0 0
     *
     * @throws SdpException
     *             SdpException, - if there is a problem constructing the
     *             SessionDescription.
     * @return a new, empty SessionDescription.
     */
    public SessionDescription createSessionDescription() throws SdpException {

        return sdpFactory.createSessionDescription();
    }

    /**
     * Creates a new SessionDescription, deep copy of another
     * SessionDescription.
     *
     * @param otherSessionDescription -
     *            the SessionDescription to copy from.
     * @return a new SessionDescription, exact and deep copy of the
     *         otherSessionDescription.
     * @throws SdpException -
     *             if there is a problem constructing the SessionDescription.
     */
    public SessionDescription createSessionDescription(
            SessionDescription otherSessionDescription) throws SdpException {
        return sdpFactory.createSessionDescription(otherSessionDescription);
    }

    /**
     * 重写Sip库方法
     *
     * Creates a SessionDescription populated with the information contained
     * within the string parameter.
     *
     * Note: unknown field types should not cause exceptions.
     *
     * @param s
     *            s - the sdp message that is to be parsed.
     * @throws SdpParseException
     *             SdpParseException - if there is a problem parsing the String.
     * @return a populated SessionDescription object.
     */
    public SessionDescription createSessionDescription(String s)
            throws SdpParseException {
        try {
            //！！！这里用自定义转换类
            SDPAnnounceParserGB28181 sdpParser = new SDPAnnounceParserGB28181(s);
            return sdpParser.parse();
        } catch (ParseException e) {
            e.printStackTrace();
            throw new SdpParseException(0, 0, "Could not parse message");
        }
//        return sdpFactory.createSessionDescription(s);
    }

    /**
     * Returns Bandwidth object with the specified values.
     *
     * @param modifier
     *            modifier - the bandwidth type
     * @param value
     *            the bandwidth value measured in kilobits per second
     * @return bandwidth
     */
    public BandWidth createBandwidth(String modifier, int value) {

        return sdpFactory.createBandwidth(modifier, value);
    }

    /**
     * Returns Attribute object with the specified values.
     *
     * @param name
     *            the namee of the attribute
     * @param value
     *            the value of the attribute
     * @return Attribute
     */
    public Attribute createAttribute(String name, String value) {
        return sdpFactory.createAttribute(name, value);
    }

    /**
     * Returns Info object with the specified value.
     *
     * @param value
     *            the string containing the description.
     * @return Info
     */
    public Info createInfo(String value) {
        return sdpFactory.createInfo(value);
    }

    /**
     * Returns Phone object with the specified value.
     *
     * @param value
     *            the string containing the description.
     * @return Phone
     */
    public Phone createPhone(String value) {
        return sdpFactory.createPhone(value);
    }

    /**
     * Returns EMail object with the specified value.
     *
     * @param value
     *            the string containing the description.
     * @return EMail
     */
    public EMail createEMail(String value) {
        return sdpFactory.createEMail(value);
    }

    /**
     * Returns URI object with the specified value.
     *
     * @param value
     *            the URL containing the description.
     * @throws SdpException
     * @return URI
     */
    public javax.sdp.URI createURI(URL value) throws SdpException {
        return sdpFactory.createURI(value);

    }

    /**
     * Returns SessionName object with the specified name.
     *
     * @param name
     *            the string containing the name of the session.
     * @return SessionName
     */
    public SessionName createSessionName(String name) {
        return createSessionName(name);
    }

    /**
     * Returns Key object with the specified value.
     *
     * @param method
     *            the string containing the method type.
     * @param key
     *            the key to set
     * @return Key
     */
    public Key createKey(String method, String key) {
        return createKey(method, key);
    }

    /**
     * Returns Version object with the specified values.
     *
     * @param value
     *            the version number.
     * @return Version
     */
    public Version createVersion(int value) {
        return createVersion(value);
    }

    /**
     * Returns Media object with the specified properties.
     *
     * @param media
     *            the media type, eg "audio"
     * @param port
     *            port number on which to receive media
     * @param numPorts
     *            number of ports used for this media stream
     * @param transport
     *            transport type, eg "RTP/AVP"
     * @param staticRtpAvpTypes
     *            vector to set
     * @throws SdpException
     * @return Media
     */
    public Media createMedia(String media, int port, int numPorts,
                             String transport, Vector staticRtpAvpTypes) throws SdpException {
        return sdpFactory.createMedia(media, port, numPorts, transport, staticRtpAvpTypes);
    }

    /**
     * Returns Origin object with the specified properties.
     *
     * @param userName
     *            the user name.
     * @param address
     *            the IP4 encoded address.
     * @throws SdpException
     *             if the parameters are null
     * @return Origin
     */
    public Origin createOrigin(String userName, String address)
            throws SdpException {
        return sdpFactory.createOrigin(userName, address);
    }

    /**
     * Returns Origin object with the specified properties.
     *
     * @param userName
     *            String containing the user that created the string.
     * @param sessionId
     *            long containing the session identifier.
     * @param sessionVersion
     *            long containing the session version.
     * @param networkType
     *            String network type for the origin (usually "IN").
     * @param addrType
     *            String address type (usually "IP4").
     * @param address
     *            String IP address usually the address of the host.
     * @throws SdpException
     *             if the parameters are null
     * @return Origin object with the specified properties.
     */
    public Origin createOrigin(String userName, long sessionId,
                               long sessionVersion, String networkType, String addrType,
                               String address) throws SdpException {
        return sdpFactory.createOrigin(userName, sessionId, sessionVersion, networkType, addrType, address);
    }

    /**
     * Returns MediaDescription object with the specified properties. The
     * returned object will respond to Media.getMediaFormats(boolean) with a
     * Vector of media formats.
     *
     * @param media
     *            media -
     * @param port
     *            port number on which to receive media
     * @param numPorts
     *            number of ports used for this media stream
     * @param transport
     *            transport type, eg "RTP/AVP"
     * @param staticRtpAvpTypes
     *            list of static RTP/AVP media payload types which should be
     *            specified by the returned MediaDescription throws
     *            IllegalArgumentException if passed an invalid RTP/AVP payload
     *            type
     * @throws IllegalArgumentException
     * @throws SdpException
     * @return MediaDescription
     */
    public MediaDescription createMediaDescription(String media, int port,
                                                   int numPorts, String transport, int[] staticRtpAvpTypes)
            throws IllegalArgumentException, SdpException {
        return sdpFactory.createMediaDescription(media, port, numPorts, transport, staticRtpAvpTypes);
    }

    /**
     * Returns MediaDescription object with the specified properties. The
     * returned object will respond to Media.getMediaFormats(boolean) with a
     * Vector of String objects specified by the 'formats argument.
     *
     * @param media
     *            the media type, eg "audio"
     * @param port
     *            port number on which to receive media
     * @param numPorts
     *            number of ports used for this media stream
     * @param transport
     *            transport type, eg "RTP/AVP"
     * @param formats
     *            list of formats which should be specified by the returned
     *            MediaDescription
     * @return MediaDescription
     */
    public MediaDescription createMediaDescription(String media, int port,
                                                   int numPorts, String transport, String[] formats) {
        return sdpFactory.createMediaDescription(media, port, numPorts, transport, formats);
    }

    /**
     * Returns TimeDescription object with the specified properties.
     *
     * @param t
     *            the Time that the time description applies to. Returns
     *            TimeDescription object with the specified properties.
     * @throws SdpException
     * @return TimeDescription
     */
    public TimeDescription createTimeDescription(Time t) throws SdpException {
        return sdpFactory.createTimeDescription(t);
    }

    /**
     * Returns TimeDescription unbounded (i.e. "t=0 0");
     *
     * @throws SdpException
     * @return TimeDescription unbounded (i.e. "t=0 0");
     */
    public TimeDescription createTimeDescription() throws SdpException {
        return sdpFactory.createTimeDescription();
    }

    /**
     * Returns TimeDescription object with the specified properties.
     *
     * @param start
     *            start time.
     * @param stop
     *            stop time.
     * @throws SdpException
     *             if the parameters are null
     * @return TimeDescription
     */
    public TimeDescription createTimeDescription(Date start, Date stop)
            throws SdpException {
        return sdpFactory.createTimeDescription(start, stop);
    }

    /**
     * Returns a String containing the computed form for a multi-connection
     * address. Parameters: addr - connection address ttl - time to live (TTL)
     * for multicast addresses numAddrs - number of addresses used by the
     * connection Returns: a String containing the computed form for a
     * multi-connection address.
     */
    public String formatMulticastAddress(String addr, int ttl, int numAddrs) {
        return sdpFactory.formatMulticastAddress(addr, ttl, numAddrs);
    }

    /**
     * Returns a Connection object with the specified properties a
     *
     * @param netType
     *            network type, eg "IN" for "Internet"
     * @param addrType
     *            address type, eg "IP4" for IPv4 type addresses
     * @param addr
     *            connection address
     * @param ttl
     *            time to live (TTL) for multicast addresses
     * @param numAddrs
     *            number of addresses used by the connection
     * @return Connection
     */
    public Connection createConnection(String netType, String addrType,
                                       String addr, int ttl, int numAddrs) throws SdpException {
        return sdpFactory.createConnection(netType, addrType, addr);
    }

    /**
     * Returns a Connection object with the specified properties and no TTL and
     * a default number of addresses (1).
     *
     * @param netType
     *            network type, eg "IN" for "Internet"
     * @param addrType
     *            address type, eg "IP4" for IPv4 type addresses
     * @param addr
     *            connection address
     * @throws SdpException
     *             if the parameters are null
     * @return Connection
     */
    public Connection createConnection(String netType, String addrType,
                                       String addr) throws SdpException {
        return sdpFactory.createConnection(netType, addrType, addr);
    }

    /**
     * Returns a Connection object with the specified properties and a network
     * and address type of "IN" and "IP4" respectively.
     *
     * @param addr
     *            connection address
     * @param ttl
     *            time to live (TTL) for multicast addresses
     * @param numAddrs
     *            number of addresses used by the connection
     * @return Connection
     */
    public Connection createConnection(String addr, int ttl, int numAddrs)
            throws SdpException {
        return sdpFactory.createConnection(addr, ttl, numAddrs);
    }

    /**
     * Returns a Connection object with the specified address. This is
     * equivalent to
     *
     * createConnection("IN", "IP4", addr);
     *
     * @param addr
     *            connection address
     * @throws SdpException
     *             if the parameter is null
     * @return Connection
     */
    public Connection createConnection(String addr) throws SdpException {

        return createConnection("IN", "IP4", addr);

    }

    /**
     * Returns a Time specification with the specified start and stop times.
     *
     * @param start
     *            start time
     * @param stop
     *            stop time
     * @throws SdpException
     *             if the parameters are null
     * @return a Time specification with the specified start and stop times.
     */
    public Time createTime(Date start, Date stop) throws SdpException {
        return sdpFactory.createTime(start, stop);
    }

    /**
     * Returns an unbounded Time specification (i.e., "t=0 0").
     *
     * @throws SdpException
     * @return an unbounded Time specification (i.e., "t=0 0").
     */
    public Time createTime() throws SdpException {
        return sdpFactory.createTime();
    }

    /**
     * Returns a RepeatTime object with the specified interval, duration, and
     * time offsets.
     *
     * @param repeatInterval
     *            the "repeat interval" in seconds
     * @param activeDuration
     *            the "active duration" in seconds
     * @param offsets
     *            the list of offsets relative to the start time of the Time
     *            object with which the returned RepeatTime will be associated
     * @return RepeatTime
     */
    public RepeatTime createRepeatTime(int repeatInterval, int activeDuration,
                                       int[] offsets) {
        return sdpFactory.createRepeatTime(repeatInterval, activeDuration, offsets);
    }

    /**
     * Constructs a timezone adjustment record.
     *
     * @param d
     *            the Date at which the adjustment is going to take place.
     * @param offset
     *            the adjustment in number of seconds relative to the start time
     *            of the SessionDescription with which this object is
     *            associated.
     * @return TimeZoneAdjustment
     */
    public TimeZoneAdjustment createTimeZoneAdjustment(Date d, int offset) {
        return sdpFactory.createTimeZoneAdjustment(d, offset);
    }

    /**
     * Test main.
     *
     * @param args
     * @throws SdpException
     *             public static void main(String[] args) throws SdpException {
     *  }
     */

    /**
     * @param ntpTime
     *            long to set
     * @return Returns a Date object for a given NTP date value.
     */
    public static Date getDateFromNtp(long ntpTime) {
        return SdpFactory.getDateFromNtp(ntpTime);
    }

    /**
     * Returns a long containing the NTP value for a given Java Date.
     *
     * @param d
     *            Date to set
     * @return long
     */
    public static long getNtpTime(Date d) throws SdpParseException {
        return SdpFactory.getNtpTime(d);
    }

}
