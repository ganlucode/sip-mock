package com.ganlucode.sipmock.gb28181.sdp;

/**
 * @author GanLu
 * @date 2020-05-27 18:23
 */
public final class SDPGBConstants {

    public static final String SSRC_FIELD = "y=";

    public static final String SDP_FIELD_PACKAGE = "com.ganlucode.sipmock.gb28181.sdp.parser";

}
