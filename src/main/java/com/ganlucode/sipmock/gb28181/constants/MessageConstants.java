package com.ganlucode.sipmock.gb28181.constants;

/**
 * @author GanLu
 * @date 2020-05-18 14:49
 */
public final class MessageConstants {

    public static final Integer DEVICE_HAS_SON_NO=0;
    public static final Integer DEVICE_HAS_SON_YES=1;

    public static final Integer DEVICE_SAFETY_DEFAULT=0;
    public static final Integer DEVICE_SAFETY_SIGN=2;
    public static final Integer DEVICE_SAFETY_ENCRYPT=3;
    public static final Integer DEVICE_SAFETY_DIGEST=4;



    public static final String DEVICE_STATUS_ON = "ON";
    public static final String DEVICE_STATUS_OFF = "OFF";



}
