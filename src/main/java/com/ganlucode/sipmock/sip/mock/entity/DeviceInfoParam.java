package com.ganlucode.sipmock.sip.mock.entity;

import com.ganlucode.sipmock.gb28181.dto.response.DeviceInfoResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GanLu
 * @date 2020-05-25 11:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceInfoParam {

    private DeviceInfoResponse deviceInfoResponse;



}
