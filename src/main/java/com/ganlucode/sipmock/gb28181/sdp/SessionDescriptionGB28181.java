package com.ganlucode.sipmock.gb28181.sdp;

import com.ganlucode.sipmock.gb28181.sdp.fields.SsrcField;
import gov.nist.javax.sdp.SessionDescriptionImpl;
import gov.nist.javax.sdp.fields.SDPField;

import java.text.ParseException;

/**
 * @author GanLu
 * @date 2020-05-27 18:19
 */
public class SessionDescriptionGB28181 extends SessionDescriptionImpl {

    protected SsrcField ssrcField;

    @Override
    public void addField(SDPField sdpField) throws ParseException {
        super.addField(sdpField);

        if (sdpField instanceof SsrcField) {
            ssrcField = (SsrcField) sdpField;
        }

    }

    public SsrcField getSsrcField() {
        return ssrcField;
    }

    public void setSsrcField(SsrcField ssrcField) {
        this.ssrcField = ssrcField;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(super.toString());
        if (ssrcField != null) {
            sb.append(ssrcField.encode());
        }
        return sb.toString();
    }
}
