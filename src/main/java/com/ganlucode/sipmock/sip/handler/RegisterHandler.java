package com.ganlucode.sipmock.sip.handler;

import com.ganlucode.sipmock.gb28181.dto.Device;
import com.ganlucode.sipmock.service.ISipCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**    
 * @Description:注册逻辑处理，当设备注册后触发逻辑。
 * @author: songww
 * @date:   2020年5月8日 下午9:41:46     
 */
@Component
public class RegisterHandler {

	@Autowired
	private ISipCommandService sipCommandService;
	
	public void onRegister(Device device) throws Exception {

		sipCommandService.deviceInfoQuery(device);

		sipCommandService.catalogQuery(device);
	}
}
