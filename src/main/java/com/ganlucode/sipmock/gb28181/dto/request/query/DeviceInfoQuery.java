package com.ganlucode.sipmock.gb28181.dto.request.query;

import com.ganlucode.sipmock.gb28181.constants.CmdType;
import com.ganlucode.sipmock.gb28181.dto.XmlBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Builder;
import lombok.Data;

/**
 * @author GanLu
 * @date 2020-05-21 14:56
 */
@Data
@Builder
@XStreamAlias("Query")
public class DeviceInfoQuery extends XmlBean {
    /**
     * 命令类型:设备目录查询(必选)
     * 默认 DeviceInfo
     */
    @Builder.Default
    @XStreamAlias("CmdType")
    protected String cmdType = CmdType.MESSAGE_DEVICE_INFO;
    /**
     * 命令序列号(必选)
     */
    @XStreamAlias("SN")
    protected String sn;
    /**
     * 目标设备/区域/联网系统编码(必选)
     */
    @XStreamAlias("DeviceID")
    protected String deviceID;

}
