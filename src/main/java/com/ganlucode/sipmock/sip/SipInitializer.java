package com.ganlucode.sipmock.sip;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.sip.SipListener;
import javax.sip.SipProvider;
import java.util.TooManyListenersException;

/**
 * @author GanLu
 * @date 2020-05-15 15:11
 */
@Slf4j
@Component
public class SipInitializer {

    @Autowired
    @Qualifier("sipListener")
    private SipListener sipListener;

    @Autowired
    @Qualifier("tcpSipProvider")
    private SipProvider tcpSipProvider;

    @Autowired
    @Qualifier("udpSipProvider")
    private SipProvider udpSipProvider;

    @Async
    void init() throws TooManyListenersException {
        log.info("添加TCP,UDP监听");
        tcpSipProvider.addSipListener(sipListener);
        udpSipProvider.addSipListener(sipListener);
    }

}
