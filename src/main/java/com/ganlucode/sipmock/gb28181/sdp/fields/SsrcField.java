package com.ganlucode.sipmock.gb28181.sdp.fields;

import com.ganlucode.sipmock.gb28181.sdp.SDPGBConstants;
import gov.nist.core.Separators;
import gov.nist.javax.sdp.fields.SDPField;
import lombok.Data;

/**
 * @author GanLu
 * @date 2020-05-27 18:22
 */
@Data
public class SsrcField extends SDPField {

    private String ssrc;

    public SsrcField() {
        super(SDPGBConstants.SSRC_FIELD);
    }

    @Override
    public String encode() {
        return SDPGBConstants.SSRC_FIELD + ssrc + Separators.NEWLINE;
    }
}
