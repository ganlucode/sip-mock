package com.ganlucode.sipmock.sip.mock;

import com.ganlucode.sipmock.sip.mock.entity.MockParam;
import com.ganlucode.sipmock.sip.mock.entity.RegisterParam;
import com.ganlucode.sipmock.sip.request.SipRequestFactory;
import com.ganlucode.sipmock.utils.SipHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.ClientTransaction;
import javax.sip.message.Request;


/**
 * @author GanLu
 * @date 2020-05-22 14:40
 */
@Service("registerMock")
public class RegisterMock implements ISipMock<RegisterParam> {

    @Autowired
    private SipHelper sipHelper;

    @Autowired
    private SipRequestFactory sipRequestFactory;

//    @Async
    @Override
    public void start(MockParam<RegisterParam> param) throws Exception {
        Request request = sipRequestFactory.createRegisterRequest(param == null ? null : param.getParam());
        ClientTransaction clientTransaction = sipHelper.getSipProvider().getNewClientTransaction(request);
        clientTransaction.sendRequest();
    }

    @Override
    public void stop(MockParam<RegisterParam> param) throws Exception {

    }
}
