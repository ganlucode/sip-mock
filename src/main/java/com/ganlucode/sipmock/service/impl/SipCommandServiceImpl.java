package com.ganlucode.sipmock.service.impl;

import com.ganlucode.sipmock.gb28181.dto.Device;
import com.ganlucode.sipmock.gb28181.dto.request.query.CatalogQuery;
import com.ganlucode.sipmock.gb28181.dto.request.query.DeviceInfoQuery;
import com.ganlucode.sipmock.service.ISipCommandService;
import com.ganlucode.sipmock.sip.request.SipRequestFactory;
import com.ganlucode.sipmock.utils.SipHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.ClientTransaction;
import javax.sip.SipException;
import javax.sip.message.Request;

/**
 * FIXME
 * @author GanLu
 * @date 2020-05-19 11:28
 */
@Slf4j
@Service
public class SipCommandServiceImpl implements ISipCommandService {

    @Autowired
    private SipHelper sipHelper;

    @Autowired
    private SipRequestFactory sipRequestFactory;

    @Override
    public boolean ptzdirectCmd(Device device, String channelId, int leftRight, int upDown) throws Exception {
        return false;
    }

    @Override
    public boolean ptzdirectCmd(Device device, String channelId, int leftRight, int upDown, int moveSpeed) throws Exception {
        return false;
    }

    @Override
    public boolean ptzZoomCmd(Device device, String channelId, int inOut) throws Exception {
        return false;
    }

    @Override
    public boolean ptzZoomCmd(Device device, String channelId, int inOut, int moveSpeed) throws Exception {
        return false;
    }

    @Override
    public boolean ptzCmd(Device device, String channelId, int leftRight, int upDown, int inOut, int moveSpeed, int zoomSpeed) throws Exception {
        return false;
    }

    @Override
    public String playStreamCmd(Device device, String channelId) throws Exception {
        return null;
    }

    @Override
    public String playbackStreamCmd(Device device, String channelId, String startTime, String endTime) throws Exception {
        return null;
    }

    @Override
    public void streamByeCmd(String ssrc) throws Exception {

    }

    @Override
    public boolean audioBroadcastCmd(Device device, String channelId) throws Exception {
        return false;
    }

    @Override
    public boolean recordCmd(Device device, String channelId) throws Exception {
        return false;
    }

    @Override
    public boolean guardCmd(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean alarmCmd(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean iFameCmd(Device device, String channelId) throws Exception {
        return false;
    }

    @Override
    public boolean homePositionCmd(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean deviceConfigCmd(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean deviceStatusQuery(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean deviceInfoQuery(Device device) throws Exception {
        DeviceInfoQuery deviceInfoQuery = DeviceInfoQuery.builder()
                .sn(String.valueOf((int) (Math.random() * 9 + 1) * 100000))
                .deviceID(device.getDeviceId())
                .build();

        String viaTag = "ViaDeviceInfoBranch";
        String fromTag = "FromDeviceInfoTag";
        String toTag = "ToDeviceInfoTag";
        Request request = sipRequestFactory.createMessageRequest(device, deviceInfoQuery, viaTag, fromTag, toTag);
        transmitRequest(device, request);
        return true;
    }

    @Override
    public boolean catalogQuery(Device device) throws Exception {
        CatalogQuery catalogQuery = CatalogQuery.builder()
                .sn(String.valueOf((int) (Math.random() * 9 + 1) * 100000))
                .deviceID(device.getDeviceId())
                .build();

        String viaTag = "ViaCatalogBranch";
        String fromTag = "FromCatalogTag";
        String toTag = "ToCatalogTag";
        Request request = sipRequestFactory.createMessageRequest(device, catalogQuery, viaTag, fromTag, toTag);
        transmitRequest(device, request);
        return true;
    }

    @Override
    public boolean recordInfoQuery(Device device, String channelId, String startTime, String endTime) throws Exception {
        return false;
    }

    @Override
    public boolean alarmInfoQuery(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean configQuery(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean presetQuery(Device device) throws Exception {
        return false;
    }

    @Override
    public boolean mobilePostitionQuery(Device device) throws Exception {
        return false;
    }


    private ClientTransaction transmitRequest(Device device, Request request) throws SipException {
        ClientTransaction clientTransaction = sipHelper.getSipProvider()
                .getNewClientTransaction(request);

        clientTransaction.sendRequest();
        return clientTransaction;
    }
}
