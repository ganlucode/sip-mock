package com.ganlucode.sipmock.sip.response.impl;

import com.ganlucode.sipmock.sip.response.ISipResponseProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sip.ResponseEvent;

/**
 * @author GanLu
 * @date 2020-05-18 17:02
 */
@Slf4j
@Service("otherResponseProcessor")
public class OtherResponseProcessor implements ISipResponseProcessor {
    @Override
    public void process(ResponseEvent responseEvent) throws Exception {

    }
}
