package com.ganlucode.sipmock.sip.request.impl;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;
import javax.sip.message.Request;

/**
 * @author GanLu
 * @date 2020-05-18 15:10
 */
@Slf4j
@Service("requestProcessorProxy")
public class RequestProcessorProxy implements ISipRequestProcessor {

    @Autowired
    @Qualifier("ackRequestProcessor")
    private ISipRequestProcessor ackRequestProcessor;

    @Autowired
    @Qualifier("byeRequestProcessor")
    private ISipRequestProcessor byeRequestProcessor;

    @Autowired
    @Qualifier("cancelRequestProcessor")
    private ISipRequestProcessor cancelRequestProcessor;

    @Autowired
    @Qualifier("inviteRequestProcessor")
    private ISipRequestProcessor inviteRequestProcessor;

    @Autowired
    @Qualifier("messageRequestProcessor")
    private ISipRequestProcessor messageRequestProcessor;

    @Autowired
    @Qualifier("otherRequestProcessor")
    private ISipRequestProcessor otherRequestProcessor;

    @Autowired
    @Qualifier("registerRequestProcessor")
    private ISipRequestProcessor registerRequestProcessor;

    @Autowired
    @Qualifier("subscribeRequestProcessor")
    private ISipRequestProcessor subscribeRequestProcessor;

    @Override
    public void process(RequestEvent evt) throws Exception {
        Request request = evt.getRequest();
        String method = request.getMethod();

        switch (method) {
            case Request.INVITE:
                inviteRequestProcessor.process(evt);
                break;
            case Request.REGISTER:
                registerRequestProcessor.process(evt);
                break;
            case Request.SUBSCRIBE:
                subscribeRequestProcessor.process(evt);
                break;
            case Request.ACK:
                ackRequestProcessor.process(evt);
                break;
            case Request.BYE:
                byeRequestProcessor.process(evt);
                break;
            case Request.CANCEL:
                cancelRequestProcessor.process(evt);
                break;
            case Request.MESSAGE:
                messageRequestProcessor.process(evt);
                break;
            default:
                otherRequestProcessor.process(evt);
        }
    }
}
