package com.ganlucode.sipmock.service;

import java.util.List;

/**
 * @author GanLu
 * @date 2020-05-18 14:36
 */
public interface ISipCacheService {

    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间（秒）
     * @return true / false
     */
    boolean expire(String key, long time);

    /**
     * 根据 key 获取过期时间
     * @param key 键
     * @return
     */
    long getExpire(String key);

    /**
     * 判断 key 是否存在
     * @param key 键
     * @return true / false
     */
    boolean hasKey(String key);

    /**
     * 删除缓存
     * 忽略类型转换警告
     * @param key 键（一个或者多个）
     */
    boolean del(String... key);

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    <T> T get(String key, Class<T> clazz);
    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    <T> List<T> getList(String key, Class<T> clazz);

    /**
     * 普通缓存放入
     * @param key 键
     * @param value 值
     * @return true / false
     */
    <T> boolean set(String key, T value);

    /**
     * 普通缓存放入并设置时间
     * @param key 键
     * @param value 值
     * @param time 时间（秒），如果 time < 0 则设置无限时间
     * @return true / false
     */
    <T> boolean set(String key, T value, long time);

    /**
     * 模糊查询
     * @param key 键
     * @return true / false
     */
    <T> List<T> keys(String key, Class<T> clazz);
}
