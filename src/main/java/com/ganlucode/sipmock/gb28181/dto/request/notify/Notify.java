package com.ganlucode.sipmock.gb28181.dto.request.notify;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GanLu
 * @date 2020-05-15 16:17
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@XStreamAlias("Notify")
public class Notify {
    @XStreamAlias("CmdType")
    protected String cmdType;
    @XStreamAlias("SN")
    protected String sn;
    @XStreamAlias("DeviceID")
    protected String deviceID;
    @XStreamAlias("Status")
    protected String status;
    @XStreamAlias("Info")
    protected String info;
}
