package com.ganlucode.sipmock.sip.request.impl.msg;

import com.ganlucode.sipmock.sip.mock.ISipMock;
import com.ganlucode.sipmock.sip.mock.MockHelper;
import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 * @date 2020-05-18 15:25
 */
@Slf4j
@Service("deviceProcessor")
public class DeviceProcessor extends BaseMessageProcessor implements ISipRequestProcessor {

    @Autowired
    private MockHelper mockHelper;

    @Autowired
    @Qualifier("deviceMock")
    private ISipMock deviceMock;
    /**
     * 收到deviceInfo设备信息请求 处理
     *
     * @param evt
     */
    @Override

    public void process(RequestEvent evt) throws Exception {
        log.info("DeviceProcessor:处理Device请求");

        deviceMock.start(mockHelper.buildDeviceInfoMock());
    }
}
