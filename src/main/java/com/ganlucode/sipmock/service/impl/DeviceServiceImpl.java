package com.ganlucode.sipmock.service.impl;

import com.ganlucode.sipmock.gb28181.dto.Device;
import com.ganlucode.sipmock.service.IDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FIXME
 * @author GanLu
 * @date 2020-05-19 11:27
 */
@Slf4j
@Service
public class DeviceServiceImpl implements IDeviceService {
    @Override
    public boolean exists(String deviceId) {
        return false;
    }

    @Override
    public boolean create(Device device) {
        return false;
    }

    @Override
    public boolean update(Device device) {
        return false;
    }

    @Override
    public Device queryVideoDevice(String deviceId) {
        return null;
    }

    @Override
    public List<Device> queryVideoDeviceList(String[] deviceIds) {
        return null;
    }

    @Override
    public boolean delete(String deviceId) {
        return false;
    }

    @Override
    public boolean online(String deviceId) {
        return false;
    }

    @Override
    public boolean outline(String deviceId) {
        return false;
    }

    @Override
    public boolean isOnline(String deviceId) {
        return true;
    }
}
