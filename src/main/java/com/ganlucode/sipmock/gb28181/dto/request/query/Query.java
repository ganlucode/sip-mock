package com.ganlucode.sipmock.gb28181.dto.request.query;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GanLu
 * @date 2020-05-19 18:03
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@XStreamAlias("Query")
@Builder
public class Query {
    @XStreamAlias("CmdType")
    protected String cmdType;
    @XStreamAlias("SN")
    protected String sn;
    @XStreamAlias("DeviceID")
    protected String deviceID;
    @XStreamAlias("StartTime")
    protected String startTim;
    @XStreamAlias("EndTime")
    protected String endTime;
}
