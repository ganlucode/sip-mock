package com.ganlucode.sipmock.sip.mock.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GanLu
 * @date 2020-05-22 14:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MockParam<T> {
    private T param;
}
