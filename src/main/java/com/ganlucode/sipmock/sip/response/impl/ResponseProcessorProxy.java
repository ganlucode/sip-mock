package com.ganlucode.sipmock.sip.response.impl;

import com.ganlucode.sipmock.sip.response.ISipResponseProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sip.ResponseEvent;
import javax.sip.header.CSeqHeader;
import javax.sip.message.Request;
import javax.sip.message.Response;

/**
 * @author GanLu
 * @date 2020-05-18 17:04
 */
@Slf4j
@Service("responseProcessorProxy")
public class ResponseProcessorProxy implements ISipResponseProcessor {

    @Autowired
    @Qualifier("byeResponseProcessor")
    private ISipResponseProcessor byeResponseProcessor;

    @Autowired
    @Qualifier("cancelResponseProcessor")
    private ISipResponseProcessor cancelResponseProcessor;

    @Autowired
    @Qualifier("inviteResponseProcessor")
    private ISipResponseProcessor inviteResponseProcessor;

    @Autowired
    @Qualifier("otherResponseProcessor")
    private ISipResponseProcessor otherResponseProcessor;

    @Override
    public void process(ResponseEvent responseEvent) throws Exception {
        Response response = responseEvent.getResponse();
        CSeqHeader cseqHeader = (CSeqHeader) response.getHeader(CSeqHeader.NAME);
        String method = cseqHeader.getMethod();
        switch (method) {
            case Request.INVITE:
                inviteResponseProcessor.process(responseEvent);
                break;
            case Request.BYE:
                byeResponseProcessor.process(responseEvent);
                break;
            case Request.CANCEL:
                cancelResponseProcessor.process(responseEvent);
                break;
            default:
                otherResponseProcessor.process(responseEvent);
        }
    }
}
