package com.ganlucode.sipmock.sip.request;

import javax.sip.RequestEvent;


/**
 * @author GanLu
 */
public interface ISipRequestProcessor {

	void process(RequestEvent evt) throws Exception;

}
