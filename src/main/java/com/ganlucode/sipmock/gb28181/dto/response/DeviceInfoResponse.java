package com.ganlucode.sipmock.gb28181.dto.response;

import com.ganlucode.sipmock.gb28181.dto.XmlBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Builder;
import lombok.Data;

/**
 * @author GanLu
 * @date 2020-05-21 10:53
 */
@Data
@Builder
@XStreamAlias("Response")
public class DeviceInfoResponse extends XmlBean {

    /**
     * 命令类型:设备信息查询(必选)
     * fixed="DeviceInfo"
     */
    @XStreamAlias("CmdType")
    protected String cmdType;
    /**
     * 命令序列号(必选)
     */
    @XStreamAlias("SN")
    protected String sn;
    /**
     * 目标设备/区域/系统的编码(必选)
     */
    @XStreamAlias("DeviceID")
    protected String deviceID;
    /**
     * 目标设备/区域/系统的名称(可选)
     */
    @XStreamAlias("DeviceName")
    protected String deviceName;
    /**
     * 查询结果(必选)
     */
    @XStreamAlias("Result")
    protected String result;
    /**
     * 设备生产商(可选)
     */
    @XStreamAlias("Manufacturer")
    protected String manufacturer;
    /**
     * 设备型号(可选)
     */
    @XStreamAlias("Model")
    protected String model;
    /**
     * 设备固件版本(可选)
     */
    @XStreamAlias("Firmware")
    protected String firmware;
    /**
     * 视频输入通道数(可选)
     */
    @XStreamAlias("Channel")
    protected String channel;
    /**
     * 扩展信息,可多项
     */
    @XStreamAlias("Info")
    protected String info;
}
