package com.ganlucode.sipmock.sip.request.impl;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 */
@Slf4j
@Service("otherRequestProcessor")
public class OtherRequestProcessor implements ISipRequestProcessor {

	/**
	 *
	 * @param evt
	 */  
	@Override
	public void process(RequestEvent evt) throws Exception {
		log.warn("no support the method! Method:{}", evt.getRequest().getMethod());
	}

}
