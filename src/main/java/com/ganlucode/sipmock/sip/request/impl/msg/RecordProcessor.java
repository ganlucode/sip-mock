package com.ganlucode.sipmock.sip.request.impl.msg;

import com.ganlucode.sipmock.sip.request.ISipRequestProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sip.RequestEvent;

/**
 * @author GanLu
 * @date 2020-05-18 15:29
 */
@Slf4j
@Service("recordProcessor")
public class RecordProcessor extends BaseMessageProcessor implements ISipRequestProcessor {


    /***
     * 
     * @param evt
     */
    @Override
    public void process(RequestEvent evt) throws Exception {
        log.info("RecordProcessor:处理Record请求");
    }
}
