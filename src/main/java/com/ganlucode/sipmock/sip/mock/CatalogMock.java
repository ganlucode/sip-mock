package com.ganlucode.sipmock.sip.mock;

import com.ganlucode.sipmock.sip.mock.entity.CatalogParam;
import com.ganlucode.sipmock.sip.mock.entity.MockParam;
import com.ganlucode.sipmock.sip.request.SipRequestFactory;
import com.ganlucode.sipmock.utils.SipHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sip.ClientTransaction;
import javax.sip.message.Request;

/**
 * @author GanLu
 * @date 2020-05-25 14:04
 */
@Service("catalogMock")
public class CatalogMock implements ISipMock<CatalogParam> {

    @Autowired
    private SipRequestFactory sipRequestFactory;

    @Autowired
    private SipHelper sipHelper;


    @Override
    public void start(MockParam<CatalogParam> param) throws Exception {
        Request request = sipRequestFactory.createCatalogRequest(param.getParam());
        ClientTransaction clientTransaction = sipHelper.getSipProvider().getNewClientTransaction(request);
        clientTransaction.sendRequest();
    }

    @Override
    public void stop(MockParam<CatalogParam> param) throws Exception {

    }
}
