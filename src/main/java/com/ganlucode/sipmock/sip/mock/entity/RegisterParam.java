package com.ganlucode.sipmock.sip.mock.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.sip.header.WWWAuthenticateHeader;

/**
 * @author GanLu
 * @date 2020-05-22 17:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterParam {

    private boolean withAuth;

    private WWWAuthenticateHeader authenticateHeader;
}
