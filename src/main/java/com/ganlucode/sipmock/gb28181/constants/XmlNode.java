package com.ganlucode.sipmock.gb28181.constants;

/**
 * @author GanLu
 * @date 2020-05-15 16:51
 */
public final class XmlNode {

    public final static String XML_VERSION = "1.0";
    public final static String XML_ENCODING = "GB2312";

    public static final String CMD_TYPE = "CmdType";

}
