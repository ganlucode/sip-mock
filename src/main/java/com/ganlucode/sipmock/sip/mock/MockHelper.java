package com.ganlucode.sipmock.sip.mock;

import com.ganlucode.sipmock.config.SipServerProperties;
import com.ganlucode.sipmock.gb28181.dto.response.CatalogResponse;
import com.ganlucode.sipmock.gb28181.dto.response.DeviceInfoResponse;
import com.ganlucode.sipmock.gb28181.dto.response.DeviceXml;
import com.ganlucode.sipmock.sip.mock.entity.CatalogParam;
import com.ganlucode.sipmock.sip.mock.entity.DeviceInfoParam;
import com.ganlucode.sipmock.sip.mock.entity.MockParam;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author GanLu
 * @date 2020-05-25 13:56
 */
@Component
public class MockHelper {

    @Autowired
    private SipServerProperties sipServerProperties;

    public DeviceInfoParam buildDeviceInfoParam(){
        DeviceInfoResponse deviceInfoResponse = DeviceInfoResponse.builder()
                .cmdType("DeviceInfo")
                .sn("400000")
                .deviceID(sipServerProperties.getSipServerId())
                .result("OK")
                .deviceName("IP CAMERA")
                .manufacturer("Hikvision")
                .model("DS-2CD3T46WD-I3")
                .firmware("V5.5.12")
                .build();
        DeviceInfoParam deviceInfoParam = new DeviceInfoParam();
        deviceInfoParam.setDeviceInfoResponse(deviceInfoResponse);
        return deviceInfoParam;
    }

    public MockParam<DeviceInfoParam> buildDeviceInfoMock(){
        return new MockParam(buildDeviceInfoParam());
    }

    public CatalogParam buildCatalogParam(){
        List<DeviceXml> deviceList = Lists.newArrayList();
        deviceList.add(DeviceXml.builder()
                .deviceId(sipServerProperties.getDeviceId())
                .name("Camera 01")
                .manufacturer("Hikvision")
                .model("IP Camera")
                .owner("Owner")
                .civilCode("CivilCode")
                .address("Address")
                .parental(0)
                .parentId(sipServerProperties.getSipServerId())
                .safetyWay(0)
                .registerWay(1)
                .secrecy(0)
                .status("ON")
                .build()
        );
        CatalogResponse catalogResponse = CatalogResponse.builder()
                .cmdType("Catalog")
                .sn("600000")
                .deviceId(sipServerProperties.getSipServerId())
                .sumNum(String.valueOf(deviceList.size()))
                .deviceList(deviceList)
                .build();
        CatalogParam catalogParam = new CatalogParam();
        catalogParam.setCatalogResponse(catalogResponse);
        return catalogParam;
    }

    public MockParam<CatalogParam> buildCatalogMock(){
        return new MockParam<>(buildCatalogParam());
    }
}
