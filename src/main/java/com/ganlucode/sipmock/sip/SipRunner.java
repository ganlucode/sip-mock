package com.ganlucode.sipmock.sip;

import com.ganlucode.sipmock.sip.mock.ISipMock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Springboot启动后执行
 * @author GanLu
 * @date 2020-05-14 19:55
 */
@Slf4j
@Component
public class SipRunner implements ApplicationRunner {

    @Autowired
    private SipInitializer sipInitializer;

    @Autowired
    @Qualifier("registerMock")
    private ISipMock registerMock;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //初始化Sip
        sipInitializer.init();
        registerMock.start(null);
    }
}
