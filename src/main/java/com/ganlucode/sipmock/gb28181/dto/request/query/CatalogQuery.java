package com.ganlucode.sipmock.gb28181.dto.request.query;

import com.ganlucode.sipmock.gb28181.constants.CmdType;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author GanLu
 * @date 2020-05-21 14:37
 */
@Data
@Builder
@XStreamAlias("Query")
public class CatalogQuery {

    /**
     * 命令类型:设备目录查询(必选)
     * 默认 Catalog
     */
    @Builder.Default
    @XStreamAlias("CmdType")
    protected String cmdType = CmdType.MESSAGE_CATALOG;
    /**
     * 命令序列号(必选)
     */
    @XStreamAlias("SN")
    protected String sn;
    /**
     * 目标设备/区域/联网系统编码(必选)
     */
    @XStreamAlias("DeviceID")
    protected String deviceID;
    /**
     * 增加设备的起始时间(可选)空表示不限
     */
    @XStreamAlias("StartTime")
    protected Date startTime;
    /**
     * 增加设备的终止时间(可选)空表示到当前时间
     */
    @XStreamAlias("EndTime")
    protected Date endTime;

}
