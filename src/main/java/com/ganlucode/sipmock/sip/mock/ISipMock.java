package com.ganlucode.sipmock.sip.mock;

import com.ganlucode.sipmock.sip.mock.entity.MockParam;

/**
 * @author GanLu
 * @date 2020-05-22 14:52
 */
public interface ISipMock<T> {

    void start(MockParam<T> param) throws Exception;

    void stop(MockParam<T> param) throws Exception;

}
